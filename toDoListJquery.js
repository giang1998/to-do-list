var CHECK = "fa-check-circle";
var UNCHECK = "fa-circle-thin";
var LINE_THROUGH = "lineThrough";
var CHANGE = "editable";
var status = "all",
    timer = null,
    delay = 260,
    click = 0;
$(document).ready(function () {
    var options = { weekday: "long", month: "short", day: "numeric" };
    var today = new Date();
    let id = 0;
    $(".clear").click(function () {
        location.reload();
    });
    $("#date").html(today.toLocaleDateString("en-US", options));
    $("#list").click(handleClickTodoItem);
    
    function addToDo(toDo, id, done, trash) {
        if (trash) return;
        var DONE = done ? CHECK : UNCHECK;
        var LINE = done ? LINE_THROUGH : "";

        var item = `<li class="item">
                        <i class="fa ${DONE} co" job="complete" id="${id}"></i>
                        <input class="text ${LINE}" job="edit" value="${toDo}" id="${id}" ">
                        <i class="fa fa-trash-o de" job="delete" id="${id}"></i>
                    </li>
                    `;
        $("#list").append(item);
    }
    $("#input").keyup(function (event) {
        if (event.keyCode === 13) {
          var toDo = $("#input").val();
          if (toDo) {
            addToDo(toDo, id, false, false);
            id++;
            $("#input").val("");
          }
        }
    });

    function handleClickTodoItem(e) {
        var element = e.target
        var job = $(element).attr('job')

        if (job === "complete") {
          completeToDo(element);
        } else if (job === "delete") {
          removeToDo(element);
        } else if (job === "edit") {
          editTodo(element);
        }
    }
    
    $(".fa-plus-circle").click(function () {
        var toDo = $("#input").val();
        if (toDo) {
          addToDo(toDo, id, false, false);
          id++;
          $("#input").val("");
        }
    });
    
    function completeToDo(element) {
        $(element).toggleClass(UNCHECK);
        $(element).toggleClass(CHECK);
        $(element).parent().children(".text").toggleClass(LINE_THROUGH);
        if (status === "active") {
          $(element).parent().remove();
        } else if (status === "completed") {
          $(element).parent().remove();
        }
    }
    
    function removeToDo(element) {
        $(element).parent().remove();
    }
    
    function editTodo(element) {
        var newTodo, content = $(element).parent().children(".text"), preText;
        click++;
        if (click === 1) {
          timer = setTimeout(function () {
            click = 0;
          }, delay);
        } else {
          click = 0;
          clearTimeout(timer);
          preText = content.val();
          content.prop("disabled", false);
          content.focus();
          content.onblur = () => {
          newTodo = content.val();
          content.prop("disabled", true);
          };
        }
    }
});
